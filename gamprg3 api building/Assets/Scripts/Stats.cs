﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    [Header("Base Values")]
    public float BaseDamage;
    public float BaseHealth;
    public float BaseMana;

    [Header("Stats")]
    public float Strength; //affects damage
    public float Endurance; //affects health
    public float Intelligence; //affects mana
    public float Agility; //affects attack (and movement??) speed

    //Health
    public float CurrentHealth { get; private set; }
    public float TotalHealth { get { return BaseHealth + Endurance * 1; } }

    //Mana
    public float CurrentMana { get; private set; }
    public float TotalMana { get { return BaseMana + Intelligence * 1; } }

    //Attack speed
    public float AttackSpeed { get { return Agility * 1; } }

    //Damage
    public float TotalDamage { get { return BaseDamage + Strength * 1; } }
    
    void Start()
    {
        //Health calculations
        CurrentHealth = TotalHealth;

        //Mana calculations
        CurrentMana = TotalMana;
    }

    public void TakeDamage(float damage)
    {
        CurrentHealth -= damage;
    }
}
