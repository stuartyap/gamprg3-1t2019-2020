﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Buff/Test")]

//Sample buff class that reduces target health, mana, and damage per tick
public class Buff_Test : Buff
{
    public float statReduction;

    public override void ApplyBuffEffect(BuffReceiver target)
    {
        target.StatsRef.BaseHealth -= statReduction * CurrentStacks;
        target.StatsRef.BaseMana -= statReduction * CurrentStacks;
        target.StatsRef.BaseDamage -= statReduction * CurrentStacks;
    }

    public override void EndBuff(BuffReceiver target)
    {
        base.EndBuff(target);
        Debug.Log("Buff expired " + Time.time);
    }
}
