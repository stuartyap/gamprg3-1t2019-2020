﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatDisplay : MonoBehaviour
{
    public Stats stat;

    public Text healthStat;
    public Text manaStat;
    public Text damageStat;
    public Text strengthStat;
    public Text enduranceStat;
    public Text intelligenceStat;
    public Text agilityStat;

    // Update is called once per frame
    void Update()
    {
        healthStat.text = stat.CurrentHealth.ToString() + " / " + stat.TotalHealth.ToString();
        manaStat.text = stat.CurrentMana.ToString() + " / " + stat.TotalMana.ToString();
        damageStat.text = stat.TotalDamage.ToString();
        strengthStat.text = stat.Strength.ToString();
        enduranceStat.text = stat.Endurance.ToString();
        intelligenceStat.text = stat.Intelligence.ToString();
        agilityStat.text = stat.Agility.ToString();
    }
}
