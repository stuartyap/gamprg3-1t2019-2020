﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemSlot {
    public Item item;
    public uint stackSize;

}

[System.Serializable]
public class Inventory
{
    private List<ItemSlot> inventory;
    private Dictionary<string, ItemSlot> invDir = new Dictionary<string, ItemSlot>();
    private uint slotSize = 10;
    

    /// <summary>
    /// checks if item will fit in the inventroy</summary>
    public bool ItemFits(Item i, uint size = 1) {
        if (invDir.ContainsKey(i.name)) {
            ItemSlot sl = invDir[i.name];
            if (sl.item.StackSize - sl.stackSize > size)
                return true;
            else { 
                uint slotCount = GetSlotCount(i,size);
                return (slotSize - inventory.Count > slotCount);
            }
        }else { 
                uint slotCount = GetSlotCount(i,size);
                return (slotSize - inventory.Count > slotCount);
            }
    }

    /// <summary>
    /// Returns slots required for item </summary>
    public uint GetSlotCount(Item i, uint size = 1){
        uint slotSize = size / i.StackSize;
        slotSize += (size % i.StackSize > 0) ? (uint)1 : (uint)0;
        return slotSize;
    }


    /// <summary>
    /// Adds an item to the inventory </summary>
    /// <returns>true if sucsessfully added</returns>
    public bool AddItem(Item i, uint size = 1) {
        if (invDir.ContainsKey(i.name))
        {
            //does the item exits in the inv
            ItemSlot sl = invDir[i.name];
            if (sl.stackSize < sl.item.StackSize){
                sl.stackSize += size;
                return true;
            }
            else{
                if (inventory.Count < slotSize){
                    NewSlot(i, size);
                    return true;
                }
                else
                    return false;
            }
        }
        else{
            // item doesn exist
            if (inventory.Count < slotSize){
                NewSlot(i,size);
                return true;
            }
            else
                return false;
        }
    }

    /// <summary>
    /// makes a new slot for the item </summary>
    /// <param name="i"></param>
    private void NewSlot(Item i, uint size = 0) {
        ItemSlot sln = new ItemSlot();
        sln.item = i;
        sln.stackSize = size;
        inventory.Add(sln);

        if (invDir.ContainsKey(i.name)) {
            invDir.Remove(i.name);
        }
        invDir.Add(i.name, sln);
        if (sln.stackSize > i.StackSize) {
            sln.stackSize = i.StackSize;
            size -= i.StackSize;
            NewSlot(i, size);
        }
    }

    /// <summary>
    /// Removes the target item from the inventory</summary>
    /// <returns>returns true if removal is sucessful</returns>
    private bool RemoveItem(Item i) {
        //gets the item slot of item
        ItemSlot sln = invDir[i.name];
        this.inventory.Remove(sln);

        invDir.Remove(i.name);

        sln = this.inventory.Find(x => x.item == i);
        //checks if there's still existing item
        if (sln != null){
            invDir.Add(i.name, sln);
        }
        return sln != null;
    }

    /// <summary>
    /// Returns the item with the given Index</summary>
    /// <returns>returns null if there's no item there</returns>
    public Item GetItemAt(int i) {
        if (i < this.inventory.Count)
            return this.inventory[i].item;
        else
            return null;
    }

    /// <summary>
    /// checks if item exists in inventory </summary>
    public bool FindItem(Item i) {
        return invDir.ContainsKey(i.name);
    }


    /// <summary>
    /// takes item from the inventory</summary>
    public bool TakeItem(Item i, uint size= 1){
        ItemSlot sln = invDir[i.name];
        if (sln != null) {
            sln.stackSize -= size;
            if (sln.stackSize < 1)
                RemoveItem(i);
            return true;
        }
        return false;
    }
    
}
