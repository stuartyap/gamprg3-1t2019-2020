﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    [Header("Stats")]
    public float Power;
    public float Precision;
    public float Toughness;
    public float Vitality;

    [SerializeField]
    private float currentHealth;

    [SerializeField]
    public float TotalHealth { get { return Vitality * 10; } }
    public float CurrentHealth { get => this.currentHealth; private set => this.currentHealth = value; }
    
    void Start()
    {
        //Health calculations
        CurrentHealth = TotalHealth;
    }

    public void TakeDamage(float damage)
    {
        //Pierre: you may wanna consider clamping this...
        //also, I'll asusme healing is negative damage
        CurrentHealth = Mathf.Clamp(CurrentHealth - damage, 0, CurrentHealth);
    }

    public void Heal(float healAmount)
    {
        CurrentHealth = Mathf.Clamp(CurrentHealth + healAmount, CurrentHealth, TotalHealth);
    }
}
