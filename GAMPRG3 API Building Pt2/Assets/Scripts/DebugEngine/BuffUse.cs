﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffUse : MonoBehaviour
{
    public BuffReceiver br;
    public Buff buff;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.enabled = false;
    }

    public void UseBuff() {
        if (buff != null)
            br.AddNewBuff(buff);
    }

}
