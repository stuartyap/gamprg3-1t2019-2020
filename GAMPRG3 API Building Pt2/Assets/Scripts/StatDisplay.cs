﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatDisplay : MonoBehaviour
{
    public Stats stat;

    public Image HealthBar;
    public Text HealthDisplay;
    public Text PowerDisplay;
    public Text PrecisionDisplay;
    public Text ToughnessDisplay;
    public Text VitalityDisplay;
    
    void Update()
    {
        HealthBar.transform.localScale = new Vector3((stat.CurrentHealth / stat.TotalHealth), 1, 1);
        HealthDisplay.text = stat.CurrentHealth + " / " + stat.TotalHealth;
        PowerDisplay.text = "Power: " + stat.Power.ToString();
        PrecisionDisplay.text = "Precision: " + stat.Precision.ToString();
        ToughnessDisplay.text = "Toughness: " + stat.Toughness.ToString();
        VitalityDisplay.text = "Vitality: " + stat.Vitality.ToString();
    }
}
