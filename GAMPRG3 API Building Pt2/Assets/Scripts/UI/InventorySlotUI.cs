﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlotUI : MonoBehaviour
{
    [SerializeField] private Text ItemName;
    [SerializeField] private Text ItemQuantity;
    [SerializeField] private Button UseButton;

    private uint stackSize;
    private uint currentSize;
    private BuffReceiver br;
    private InventoryGO inventoryRef;


    [HideInInspector] public ItemSlot slotRef;

    public BuffReceiver Br { get => br; set => br = value; }
    public InventoryGO InventoryRef { get => inventoryRef; set => inventoryRef = value; }

    //public void SetSlot()
    //{
    //    ItemName.text = slotRef.item.ItemName;
    //    ItemQuantity.text = slotRef.ToString();

    //    if (!slotRef.item.Usable) UseButton.interactable = false;
    //    //else UseButton.onClick.AddListener();
    //}

    public void SetSlot(string name, uint stackSize){
        ItemName.text = name;
        ItemQuantity.text = currentSize.ToString();
    }

    public void UpdateSlot() {
        if (slotRef != null) { 
            if (slotRef.stackSize == 0)
            {
                slotRef = null;
            }
        }
        ItemName.text = (slotRef != null)? slotRef.item.name: "empty";
        ItemQuantity.text = (slotRef != null) ? slotRef.stackSize.ToString():"0";
        UseButton.gameObject.SetActive((slotRef != null) ? slotRef.item.Usable : false);
        if (UseButton.gameObject.activeSelf)
        {
            UseButton.onClick.RemoveAllListeners();
            UseButton.onClick.AddListener(this.UseItem);
            UseButton.onClick.AddListener(this.ConsumeSelf);
            UseButton.onClick.AddListener(this.UpdateSlot);
        }
        
    }

    private void UseItem() {
        if (this.slotRef != null) { 
            if (this.slotRef.item is Consumable) {
                Consumable c = (Consumable)this.slotRef.item;
                c.UseItem(this.br);
            }
        }
    }
    private void ConsumeSelf() {
        if (this.slotRef != null)
        { 
            this.inventoryRef.ConsumeItem(this.slotRef.item);
        }
    }

}
