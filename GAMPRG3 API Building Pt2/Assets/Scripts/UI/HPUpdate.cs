﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPUpdate : MonoBehaviour
{
    public Stats stats;
    [SerializeField]
    private Text txt;
    private float prevValue = 0;
    // Start is called before the first frame update
    void Start()
    {
        txt = GetComponent<Text>();
        UpdateUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (prevValue != GetHP())
            UpdateUI();
    }

    private void UpdateUI()
    {
        prevValue = GetHP();
        txt.text = prevValue.ToString();
    }

    private float GetHP()
    {
        return stats.CurrentHealth;
    }
}
