﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
    public GameObject SlotPrefab; //Slot panel prefab to be instantiated
    public InventoryGO InventoryRef = null;
    public BuffReceiver br;
    //public int Size;

    private List<InventorySlotUI> slots;

    void Start()
    {
        slots = new List<InventorySlotUI>();
        if(InventoryRef)
        InitializeSlots((int)InventoryRef.Inventory.SlotSize);
    }

    void Update()
    {
        //UpdateSlots();
    }

    private void InitializeSlots(int size){
        for (int i = 0; i < size; i++){
            slots.Add(AddNewSlot());
        }
        
    }

    //Creates a new item slot with an item's values
    private InventorySlotUI AddNewSlot(ItemSlot slot){
        GameObject newSlot = Instantiate(SlotPrefab, transform);

        InventorySlotUI slotComponent = newSlot.GetComponent<InventorySlotUI>();
        slotComponent.slotRef = slot;
        UpdateSlots();
        return slotComponent;
    }

    //Creates a new empty item slot
    private InventorySlotUI AddNewSlot(){
        GameObject newSlot = Instantiate(SlotPrefab, transform);
        InventorySlotUI slotComponent = newSlot.GetComponent<InventorySlotUI>();
        UpdateSlots();
        return slotComponent;
    }
    
    private void UpdateSlots(){
        //print("slots count: " + slots.Count);
        for (int i = 0; i < slots.Count; i++){
            ItemSlot slot = this.InventoryRef.Inventory.GetSlotAt(i);
            if (slots[i].InventoryRef == null)
                slots[i].InventoryRef = this.InventoryRef;
            if (slots[i].Br == null)
                slots[i].Br = this.br;
            slots[i].slotRef = slot;
            slots[i].UpdateSlot();
        }
    }

    private void UpdateSlots(int start, int end) {
        for (int i = start; i <= end; i++){
            ItemSlot slot = this.InventoryRef.Inventory.GetSlotAt(i);
            if (slots[i].InventoryRef == null)
                slots[i].InventoryRef = this.InventoryRef;
            if (slots[i].Br == null)
                slots[i].Br = this.br;
            slots[i].slotRef = slot;
            slots[i].UpdateSlot();
        }
    }
}
