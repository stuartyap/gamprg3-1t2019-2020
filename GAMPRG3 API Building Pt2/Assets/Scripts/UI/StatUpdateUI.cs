﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatUpdateUI : MonoBehaviour
{
    public Stats targ;
    public StatsTarg stat;
    private float prevValue = -1f;
    [SerializeField]
    private Text txt;
    private string format = "";

    // Start is called before the first frame update
    void Start()
    {
        prevValue = GetStat();
        if (!txt)
            txt = GetComponent<Text>();
        format = txt.text;
        prevValue = GetStat();
        txt.text = string.Format(format, prevValue);
    }

    // Update is called once per frame
    void Update()
    {
        if (prevValue != GetStat()) {
            prevValue = GetStat();
            txt.text = string.Format(format, prevValue);
        }
    }
    private float GetStat() {
        return StatBuff.GetStat(targ, stat);
    }
}
