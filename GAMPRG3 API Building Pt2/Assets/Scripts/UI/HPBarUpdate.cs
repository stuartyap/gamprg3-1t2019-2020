﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBarUpdate : MonoBehaviour
{
    public Stats stats;
    [SerializeField]
    private Image bar;
    private float prevValue = 0;
    // Start is called before the first frame update
    void Start()
    {
        if (!bar)
            bar = GetComponent<Image>();
        UpdateUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (prevValue != GetHPNorm())
            UpdateUI();
    }

    private void UpdateUI() {
        prevValue = GetHPNorm();
        bar.fillAmount = prevValue;
    }

    private float GetHPNorm() {
        return stats.CurrentHealth / stats.TotalHealth;
    }
}
