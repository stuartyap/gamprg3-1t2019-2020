﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffReceiver : MonoBehaviour
{
    public Stats StatsRef;
    public Dictionary<string, Buff> Buffs = new Dictionary<string, Buff>();

    void Start()
    {
        if (StatsRef == null) StatsRef = GetComponent<Stats>();
    }

    void Update()
    {
        foreach (Buff b in Buffs.Values)
        {
            if (b.InfiniteDuration) return;
            b.lifetime += Time.deltaTime;

            //Destroy the buff once its duration ends
            if (b.lifetime >= b.Duration)
            {
                b.EndBuff(this);
                Buffs.Remove(b.BuffName);
                //Destroy(b);
            }
        }
    }

    public void AddNewBuff(Buff newBuff)
    {
        //If the buff already exists in the dictionary
        if (Buffs.ContainsKey(newBuff.BuffName))
        {
            Buff existingBuff = Buffs[newBuff.BuffName];

            switch (newBuff.StackType)
            {
                //Destroy the buff
                case BuffStackType.NotStackable:
                    Destroy(newBuff);
                    return;

                //Increase the duration of the existing buff
                case BuffStackType.Duration:
                    existingBuff.Duration += newBuff.Duration;
                    //Destroy(newBuff);
                    return;
                
                //Increase the stack count and add the buff to the dictionary
                case BuffStackType.Intensity:
                    if (existingBuff.MaxStacksReached) Destroy(newBuff);
                    else
                    {
                        //Remove the old buff and pass everything to the new buff
                        newBuff.CurrentStacks += existingBuff.CurrentStacks;
                        Buffs.Remove(existingBuff.BuffName);
                        StartCoroutine(HandleOldIntensityStack(existingBuff, existingBuff.Duration - existingBuff.lifetime));

                        //Add the new buff to the dictionary
                        Buffs.Add(newBuff.BuffName, newBuff);
                        newBuff.StartBuff(this);
                        StartCoroutine(ApplyBuffPerInterval(newBuff));
                    }
                    return;

                default:
                    return;
            }

        }

        //Else add it to the dictionary
        else
        {
            Buffs.Add(newBuff.BuffName, newBuff);
            newBuff.StartBuff(this);
            StartCoroutine(ApplyBuffPerInterval(newBuff));
        }
    }
    
    IEnumerator ApplyBuffPerInterval(Buff buff)
    {
        buff.ApplyBuffEffect(this);

        yield return new WaitForSeconds(buff.Interval);

        //Start another interval while the buff is still active
        if (buff != null) StartCoroutine(ApplyBuffPerInterval(buff));
    }

    //Manages old intensity stacks after they get replaced by new ones
    private IEnumerator HandleOldIntensityStack(Buff b, float remainingTime)
    {
        b.CurrentStacks = 1;
        yield return new WaitForSeconds(remainingTime);
        Buffs[b.BuffName].CurrentStacks--;
        b.EndBuff(this);
        //Destroy(b);
    }
}
