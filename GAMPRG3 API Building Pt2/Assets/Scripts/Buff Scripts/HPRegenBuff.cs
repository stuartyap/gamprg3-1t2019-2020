﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Buff/HPRegenBuff", order = 1)]
public class HPRegenBuff : Buff
{
    [Header("Health")]
    public float regenFactor = 1f;
    private Stats targetStats = null;
    public override void ApplyBuffEffect(BuffReceiver target)
    {
        //step 1: get HP
        //step 2: apply regen
        CheckStats(target);

        float regen = targetStats.TotalHealth;
        regen *= regenFactor;
        targetStats.Heal(regen);
    }

    private void CheckStats(BuffReceiver target) {
        if (!targetStats)
            targetStats = target.StatsRef;
    }

}
