﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuffStackType
{
    NotStackable, Duration, Intensity
}


[CreateAssetMenu(menuName = "Buff/BasicBuff", order = 1)]
[System.Serializable]
public class Buff : ScriptableObject
{
    public string BuffName;

    public BuffStackType StackType;

    //DURATION
    [Header("Duration Properties")]
    public bool InfiniteDuration;
    public float Duration; //how long does the buff last
    public float Interval = 1; //how long is the time gap between ticks (default 1 second)
    [HideInInspector] public float lifetime; //time passed since the buff was instantiated (starts at 0)

    //INTENSITY
    [Header("Stacking Properties")]
    public int MaxStacks = 1;
    [HideInInspector] public int CurrentStacks = 1; //default 1 because this buff instance already counts
    public bool MaxStacksReached { get { return (CurrentStacks >= MaxStacks); } }

    public Buff()
    {
        lifetime = 0;
    }

    //Buff effect per tick
    public virtual void ApplyBuffEffect(BuffReceiver target) { }
    //Buff effect on start
    public virtual void StartBuff(BuffReceiver target) { }
    //Buff effect on end
    public virtual void EndBuff(BuffReceiver target) { }
}
