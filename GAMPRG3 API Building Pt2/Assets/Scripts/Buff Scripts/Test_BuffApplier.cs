﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_BuffApplier : MonoBehaviour
{
    public Buff buff;
    public BuffReceiver buffReceiver;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            buffReceiver.AddNewBuff(Instantiate(buff));
        }
    }
}
