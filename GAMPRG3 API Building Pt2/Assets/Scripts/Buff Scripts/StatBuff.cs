﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StatsTarg {
    POW,PRC,TUF,VIT
}

[System.Serializable]
[CreateAssetMenu(menuName = "Buff/StatBuff", order = 1)]
public class StatBuff : Buff
{
    [Header("Stat Buff Options")]
    public float value = 1f;
    public bool isMultiply = true;
    public StatsTarg targetStat;
    //Buff effect on start
    public override void StartBuff(BuffReceiver target) {
        if (isMultiply)
            StatBuff.GetStat(target.StatsRef, this.targetStat) *= value;
        else
            StatBuff.GetStat(target.StatsRef, this.targetStat) += value;
    }
    //Buff effect on end
    public override void EndBuff(BuffReceiver target) {
        if (isMultiply)
            StatBuff.GetStat(target.StatsRef, this.targetStat) /= value;
        else
            StatBuff.GetStat(target.StatsRef, this.targetStat) -= value;
    }

    public static ref float GetStat(Stats s, StatsTarg st) {
        switch (st) {
            default:
                return ref s.Power;
            case StatsTarg.PRC:
                return ref s.Precision;
            case StatsTarg.TUF:
                return ref s.Toughness;
            case StatsTarg.VIT:
                return ref s.Vitality;
        }
    }



}
