﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item/Consumable/Weird Mushroom")]
public class WeirdMushroom : Consumable
{
    public List<Buff> RandomEffects;

    public Buff GetRandomEffect()
    {
        return (RandomEffects[Random.Range(0, RandomEffects.Count - 1)]);
    }

    public override void UseItem(BuffReceiver target)
    {
        target.AddNewBuff(GetRandomEffect());
    }
}
