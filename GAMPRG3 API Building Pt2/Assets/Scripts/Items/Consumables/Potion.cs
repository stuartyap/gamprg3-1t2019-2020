﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item/Consumable/Potion")]
public class Potion : Consumable
{
    public Buff PotionEffect;

    public override void UseItem(BuffReceiver target)
    {
        Debug.Log(this.PotionEffect.BuffName);
        Debug.Log(target.gameObject.name);
        target.AddNewBuff(PotionEffect);

    }
}
