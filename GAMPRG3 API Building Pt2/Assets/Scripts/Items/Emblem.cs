﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item/Emblem")]
public class Emblem : Item
{
    public StatBuff buff;
}
