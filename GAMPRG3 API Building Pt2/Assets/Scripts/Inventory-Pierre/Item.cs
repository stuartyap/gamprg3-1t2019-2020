﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item : ScriptableObject
{
    [SerializeField]
    private string itemName;
    [SerializeField]
    private bool usable;
    [SerializeField]
    private uint stackSize = 0;

    public string ItemName { get => itemName; }
    public bool Usable { get => usable; set => usable = value; }
    public uint StackSize { get => stackSize; set=> stackSize = value;}
}
