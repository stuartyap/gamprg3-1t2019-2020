﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemComponent : MonoBehaviour
{
    [SerializeField]
    private Item itemData = null;

    public Item ItemData { get => itemData; set => itemData = value; }

    // Update is called once per frame
    void Update()
    {
        this.enabled = false;
    }
}
