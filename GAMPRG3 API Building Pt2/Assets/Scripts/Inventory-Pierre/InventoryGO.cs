﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryGO : MonoBehaviour
{
    [SerializeField]
    private BuffReceiver br;
    [SerializeField]
    private Inventory inventory = null;
    [SerializeField]
    private uint slotSize;

    public Inventory Inventory { get => inventory; }
    public BuffReceiver Br { get => br; set => br = value; }

    // Start is called before the first frame update
    void Start()
    {
        if (inventory == null)
            this.inventory = new Inventory();
        this.inventory.SlotSize = this.slotSize;
        this.inventory.StartWithItems();
    }

    // Update is called once per frame
    void Update(){
        this.enabled = false;
    }

    public void ConsumeItem(Item i) {
        this.inventory.TakeItem(i);
    }

}
